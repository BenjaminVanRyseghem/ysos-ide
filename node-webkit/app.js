requirejs.config({
	baseUrl: 'js',
	paths:   {
		'jQuery':    'lib/jQuery/jquery.min',
		'jquery':    'lib/jQuery/jquery.min',
		'ysos':      'lib/ysos/js',
		'widgetjs':  'lib/widgetjs/src',
		'mousetrap': 'lib/mousetrap-1.4.6/mousetrap.min'
	}
});

// Start the main app logic.
requirejs([
		'ysos/loadYsos',
		'widgetjs/core',
		'widgetjs/events',
		'widgetjs/htmlCanvas',
		'jQuery',
		'mousetrap'
	],
	function(loadYsos) {

		if(requireNode){
			window.FileSystem = requireNode('fs');
		}

		loadYsos({
			pathToKernel: 'js/lib/ysos/'
		});
	}
);